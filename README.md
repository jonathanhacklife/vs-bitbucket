# VS BitBucket

This theme is inspired by the default VSCode theme.
Ideal for those who want to highlight their code in the code review or Pull Request diffs.

_Minimal changes for now, Full dark theme coming soon 🌠_
