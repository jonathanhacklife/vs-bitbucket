# Changelog

## 1.0.1 [2021-10-21]
### Improved
- Updated colors for commit difference
- Correction of the color difference display to darker tones


## 1.0.0 [2021-10-21]
### Init
- Added better colors for difference display
- Added better styles for some button events
- Difference viewers are styled with default VSCode styling.

